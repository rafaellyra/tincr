var projectTypes;
        
var checkProjectState = function(){
    var projectState = window.devtoolsWindow.projectState;
        
    if (projectState.type == 'fileUrl'){
        $('#project-type').attr('disabled', 'true');
        $('#native-browse').attr('disabled', 'true');
        
        $('#file-select').hide();
        $('#auto-refresh').hide();
        $('#auto-save').hide();
    }
    else if (projectState.type){
        var typeSelect = document.getElementById('project-type');
        $('#project-type').removeAttr('disabled');
        $('#native-browse').removeAttr('disabled');
        
        for (var i = 0; i < projectTypes.length; i++){
            if (projectTypes[i].key === projectState.type){
                typeSelect.value = String(i);
                break;
            }
        }
        $('#local-file-path').text(projectState.path).show();
        $('#toggle-watch')[0].checked = projectState.watchFiles;
        $('#toggle-autosave')[0].checked = projectState.autosave;
        
        $('#file-select').show();
        $('#auto-refresh').show();
        $('#auto-save').show();
        
        $('#load-error').hide();
        $('#error-img').hide();
        $('#success-img').show();
        $('#load-success').show();
    }
    
    chrome.storage.local.get('logging', function(data){
        var loggingEnabled = data['logging'] === 'true';
        toggleLogging(loggingEnabled);
        $('#toggle-logging')[0].checked = loggingEnabled;
    })
    
    
}
var clearResources = function(){
    $('#resource-list-container').empty();
};
var updateResource = function(resource, isUserMapped){
    var paths = resource.paths || {};
    var $item = $('.resource[data-url="' + resource.url +'"]');
    updateResourceData(resource.url, $item, paths, isUserMapped);
};

var updateResourceData = function(url, $item, paths, isDocument, isUserMapped){
    if (isUserMapped){
        $item.addClass('user-mapped');
    }
    else{
        $item.removeClass('user-mapped');
    }

    if (paths.autoSave){
        $('.resource-localpath span', $item).text(paths.autoSave);
    }
    else if (paths.autoReload && paths.autoReload.length){
        $('.resource-localpath span', $item).text(paths.autoReload[0]);   
    }
    $('.auto-refresh-item', $item).remove();
    $('.resource-reloadfrom', $item).hide();
    if (paths.autoReload && paths.autoReload.length > 1){ 
        $('.resource-reloadfrom', $item).show();
        for (var j = 1; j < paths.autoReload.length; j++){
            $('.resource-reloadfrom ul', $item).append('<li class="auto-refresh-item">' + paths.autoReload[j] + '&nbsp;<a data-index="' + j + '" href="#" class="remove-file">Remove</a></li>');
        }
    }
    updateResourceActions(url, $item, paths, isDocument);
    updateResourceOptions(url, $item, paths, isDocument);

};

var updateResourceActions = function(url, $item, paths, isDocument){
    var $actions = $('.resource-actions', $item);
    $actions.empty();

    if (paths.autoReload || paths.autoSave){
        $actions.append('<a href="#" class="browse-autosave-file">Change File Path</a>');
        if (paths.autoReload && paths.autoReload.length){
           $actions.append('<span> | </span><a href="#" class="browse-add-autorefresh-file">Add Dependency</a>'); 
        } 
    }
    else{
        $actions.append('<a href="#" class="browse-autosave-file">Set File Path</a>');
    }

    var data = paths;
    $('.browse-autosave-file', $item).click(function(){
        backgroundMsgSupport.launchFileSelect(url, 'autosave', function(data){
            //$('.resource-localpath span', $item).text(data.localPath);
            updateResourceData(url, $item, data, isDocument, true);
            window.devtoolsWindow.saveMapping(url, data);
        });
        return false;
    });

    // deal with adding a file for auto refresh
    $('.browse-add-autorefresh-file', $item).click(function(){
        backgroundMsgSupport.launchFileSelect(url, 'add-autorefresh', function(data){
            updateResourceData(url, $item, data, isDocument, true);
            window.devtoolsWindow.saveMapping(url, data);
        });
        return false;
    });

    $('.remove-file', $item).click(function(){
        backgroundMsgSupport.removeDependency(url, $(this).attr('data-index'), function(data){
            updateResourceData(url, $item, data, isDocument, true);
            window.devtoolsWindow.saveMapping(url, data);
        });
        return false;
    });
}

var updateResourceOptions = function(url, $item, paths, requiresFull){
    var $options = $('.resource-options', $item);

    if (paths.autoSave){
        //$('.auto-refresh-checkbox', $options).prop('checked', false);
        $('.auto-refresh-checkbox', $options).prop('checked', false);
        $('input', $options).prop('disabled', false);
    }
    else if (paths.autoReload && paths.autoReload.length){
        //$('.auto-save-checkbox', $options).prop('checked', false);
        $('.auto-refresh-checkbox', $options).prop('checked', true);
        $('input', $options).prop('disabled', false);
    }
    else{
        $('.auto-refresh-checkbox', $options).prop('checked', false).prop('disabled', true);
        //$('.auto-save-checkbox', $options).prop('checked', false).prop('disabled', true);
        $('.full-refresh-checkbox', $options).prop('disabled', true);
    }
    if (window.devtoolsWindow.isFullReload(url)){
        $('.full-refresh-checkbox', $options).prop('checked', true);
    }
    else{
        $('.full-refresh-checkbox', $options).prop('checked', false);
    }
    if (requiresFull){
         $('.full-refresh-checkbox', $options).prop('checked', true).prop('disabled', true);
    }
}

var registerOptionChangeHandlers = function(url, $item, requiresFull){
    var $autoRefeshCheckbox = $('.auto-refresh-checkbox', $item);
    var $fullRefreshCheckbox = $('.full-refresh-checkbox', $item);
    var updateAll = function(){
        backgroundMsgSupport.setResourceOptions(url, !$autoRefeshCheckbox[0].checked, function(paths){
            updateResourceData(url, $item, paths, requiresFull, true);
            window.devtoolsWindow.saveMapping(url, paths);
        });
        return false;
    };
    $autoRefeshCheckbox.click(updateAll);
    $fullRefreshCheckbox.change(function(){
        window.devtoolsWindow.saveFullReloadMapping(url, this.checked);
    });
}

var refreshResources = function(resourceCache){
    var urlParser = document.createElement('a');
    var inspectedOrigin = devtoolsWindow.inspectedLocation.origin;
    var $resourceList = $('#resource-list-container');
    for (var i = 0; i < resourceCache.length; i++){
        var resource = resourceCache[i];
        var type = resource.type;
        if ((type != 'document' && type != 'script' && type != 'stylesheet') || resource.url.indexOf(inspectedOrigin) != 0){
            continue;
        }
        var template = '<div class="resource ' + type +'">\
            <div class="details">\
                <div class="resource-name">test.js</div>\
                <div class="resource-url">Url: /something/something/test.js</div>\
                <div class="resource-localpath">File Path: <span>None</span></div>\
                <div style="display:none" class="resource-reloadfrom">\
                    <ul>\
                    </ul>\
                </div>\
                <div class="resource-actions">\
                    <a href="#" class="browse-autosave-file">Set File</a>\
                </div>\
                <div class="resource-options">\
                    <label>\
                        <input name="resource-match-type' + i + '" class="auto-refresh-checkbox" type="checkbox" />\
                        <span>Generated from another language</span>\
                    </label>\
                    <label>\
                        <input class="full-refresh-checkbox" type="checkbox" />\
                        <span>Auto-Refresh performs a full refresh of the page</span>\
                    </label>\
                </div>\
            </div>\
        </div>';

        urlParser.href = resource.url;
        var filename = urlParser.pathname.substring(urlParser.pathname.lastIndexOf('/') + 1);
        var url = urlParser.origin === inspectedOrigin ? resource.url.substring(inspectedOrigin.length) : resource.url;
        var $item = $(template);
        $item.appendTo($resourceList);
        
        $item.attr('data-url', resource.url);
        $('.resource-name', $item).text(filename.length ? filename : '/');
        $('.resource-url', $item).text('URL: ' + url);

        var paths = resource.paths || {};
        updateResourceData(resource.url, $item, paths, type == 'document', false);
        //updateResourceActions($item, paths);
        //updateResourceOptions($item, paths, type == 'document');
        
        registerOptionChangeHandlers(resource.url, $item, type == 'document');
    }
};

var initUI = function(){

    var typeSelect = document.getElementById('project-type');
    
    backgroundMsgSupport.getProjectTypes(function(types){
        projectTypes = types;
        for (var i = 0; i < projectTypes.length; ++i) {
            var projectType = projectTypes[i];
            typeSelect.add(new Option(projectType.name, i));
        }
        checkProjectState();
    });
    $(typeSelect).on('change', function(e){
        var index = Number(typeSelect.value);
        var projectType = projectTypes[index];
        if (projectType.locationType == 'local'){
            $('#file-select').show();
        }
    });
    $('#native-browse').on('click', function(e){
        var index = Number(typeSelect.value);
        backgroundMsgSupport.launchFolderSelect(index, window.devtoolsWindow.inspectedLocation.origin, function(result){
            if (result.path && result.path.length){
                $('#local-file-path').text(result.path).show();
                if (result.error){
                    logError(result.error);
                    $('#load-error').text(result.error).show();
                    $('#error-img').show();
                    $('#success-img').hide();
                    $('#load-success').hide();
                }
                else{
                    var projectType = projectTypes[index];
                    window.devtoolsWindow.loadProject(projectType.key, result.path, true, true, {});
                    $('#toggle-autosave')[0].checked = true;
                    $('#toggle-watch')[0].checked = true;
                    $('#auto-refresh').show();
                    $('#auto-save').show();
                    
                    $('#load-error').hide();
                    $('#error-img').hide();
                    $('#success-img').show();
                    $('#load-success').show();
                }
            }
        });
        return false;
    });
    $('#toggle-watch').on('change', function(e){
        var path = $('#local-file-path').text();
        if (path && path.length){
            this.disabled=true;
            var self = this;
            window.devtoolsWindow.toggleWatchingFiles(this.checked, path, function(){
                self.disabled = false;
                window.devtoolsWindow.saveProjectState();
            });
        }
    });
    $('#toggle-autosave').on('change', function(e){
        if (window.devtoolsWindow.projectState){
            window.devtoolsWindow.projectState.autosave = this.checked;
        }
        window.devtoolsWindow.saveProjectState();
    });
    $('#toggle-logging').on('change', function(e){
        chrome.storage.local.set({'logging': this.checked});
        toggleLogging(this.checked);
        devtoolsWindow.toggleLogging(this.checked);
    });

    
    refreshResources(window.devtoolsWindow.resourceCache)
    //$('#resource-list-container').list()

};