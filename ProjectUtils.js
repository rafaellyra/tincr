var ProjectUtils = {
    scanDirectory : function(root, path, onFile, onEnd){

        var walkTree = function(dir, callback){
            Gito.FileUtils.ls(dir, function(entries){
                entries.asyncEach(function(entry, done){
                    if (entry.isFile){
                        onFile(entry);
                        done();
                    }
                    else{
                        walkTree(entry, done);
                    }
                }, callback);
            });
        }
        if (path){
            root.getDirectory(path, {create:false}, function(dir){
                walkTree(dir, onEnd);
            }, onEnd);
        }
        else
        {
            walkTree(root, onEnd);
        }
    },
    stripQueryString : function(rawUrl){
        var url = rawUrl.split("?")[0];
        return url;
    },
    clearUrlCache : function(rawUrl, stripQuery, urlCache, fileCache){
        var url = stripQuery ? ProjectUtils.stripQueryString(rawUrl) : rawUrl;
        var data = urlCache[url];
        if (data && data.autoReload){
            for (var i = 0; i < data.autoReload.length; i++){
                delete fileCache[data.autoReload[i]];
            }
        }
    },
    cacheRelationship : function(rawUrl, obj, stripQuery, urlCache, fileCache){
        url = stripQuery ? ProjectUtils.stripQueryString(rawUrl) : rawUrl;
        urlCache[url] = obj;
        if (obj.autoSave){
            fileCache[obj.autoSave] = rawUrl;
        }
        else if (obj.autoReload){
            for (var i = 0; i < obj.autoReload.length; i++){
                fileCache[obj.autoReload[i]] = rawUrl;
            }
        }
    }  
};